FROM golang

ENV GO111MODULE=on

ADD . /go/src/github.com/google/web-api-gateway

WORKDIR /go/src/github.com/google/web-api-gateway

RUN go install /go/src/github.com/google/web-api-gateway/server
RUN go install /go/src/github.com/google/web-api-gateway/setuptool
RUN go install /go/src/github.com/google/web-api-gateway/connectiontest

ENTRYPOINT ["/go/bin/server"]

EXPOSE 443
